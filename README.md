# DiscreteSlider
**本项目是基于开源项目DiscreteSlider进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址（https://github.com/hearsilent/DiscreteSlider）追踪到原安卓项目版本**

#### 项目介绍
DiscreteSlider tries to offer you the slider with value label as shown in the [Material Design spec], with an API. The library also offer you range slider mode.

DiscreteSlider尝试为您提供带有“材质设计”规范中所示的值标签的滑块，以及一个API。该库还为您提供范围滑块模式


## Screenshot
<img src="https://raw.githubusercontent.com/hearsilent/DiscreteSlider/master/screenshots/screenrecord.gif" height="500">

### Normal Mode
<img src="https://raw.githubusercontent.com/hearsilent/DiscreteSlider/master/screenshots/device-2019-04-10-215937.png" height="500">

### Range Mode
<img src="https://raw.githubusercontent.com/hearsilent/DiscreteSlider/master/screenshots/device-2019-04-10-215451.png" height="500">

### Vertical Orientation
<img src="https://raw.githubusercontent.com/hearsilent/DiscreteSlider/master/screenshots/device-2019-04-15-221115.png" height="500">


## Setup
   
The easiest way to add the **DiscreteSlider** library to your project is by adding it as a dependency to your build.gradle

**Step 1.** Add the dependency
```gradle
添加库的依赖
方式一：
添加har包到lib文件夹内
在entry的gradle内添加如下代码
implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])

方式二：
allprojects {
    repositories{
        mavenCentral()
    }
}
implementation 'io.openharmony.tpc.thirdlib:DiscreteSlider:1.0.1'
```
   
## Usage
   
Setup **Track** in Java
```java
mSlider.setTrackWidth(Utils.convertDpToPixel(4, this));
mSlider.setTrackColor(0xFFD81B60);
mSlider.setInactiveTrackColor(0x3DD81B60);
```
   
Setup **Track** in Xml
```xml
app:ds_trackWidth="4vp"
app:ds_trackColor="#D81B60"
app:ds_inactiveTrackColor="#3DD81B60"
```
   
Setup **Thumb** in Java
```java
mSlider.setThumbRadius(Utils.convertDpToPixel(6, this));
mSlider.setThumbColor(0xFFD81B60);
mSlider.setThumbPressedColor(0x1FD81B60);
```
   
Setup **Thumb** in Xml
```xml
app:ds_thumbRadius="6vp"
app:ds_thumbColor="#D81B60"
app:ds_thumbPressedColor="#1FD81B60"
```
   
Setup **TickMark** in Java
```java
mSlider.setTickMarkColor(0x3DFFFFFF);
mSlider.setTickMarkInactiveColor(0x1FD81B60);
mSlider.setTickMarkPatterns(Arrays.asList(new Dot(), new Dash(Utils.convertDpToPixel(1, this))));
// TickMark step must be a factor of (count - 1)
mSlider.setTickMarkStep(1);
```
   
Setup **TickMark** in Xml
```xml
app:ds_tickMarkColor="#3DFFFFFF"
app:ds_tickMarkInactiveColor="#1FD81B60"
<!-- Separated by commas, only accept `dot` or `dash`. You can also fill in "dot,dash,dash" -->
app:ds_tickMarkPatterns="dot,dash"
app:ds_tickMarkDashLength="1dp"
<!-- TickMark step must be a factor of (count - 1) -->
app:ds_tickMarkStep="1"
```
   
Setup **ValueLabel** in Java
```java
mSlider.setValueLabelTextColor(Color.WHITE);
mSlider.setValueLabelTextSize(Utils.convertSpToPixel(16, this));
mSlider.setValueLabelGravity(DiscreteSlider.TOP);
// Customize value label's text by `setValueLabelFormatter`
mSlider.setValueLabelFormatter(new DiscreteSlider.ValueLabelFormatter() {

    @Override
    public String getLabel(int input) {
        return Integer.toString(input);
    }
});
mSlider.setValueLabelMode(1); // 0: none, 1: showOnPressHold, 2: showOnProgressChange, 3: showOnPressHold & showOnProgressChange
mSlider.setValueLabelDuration(1500); // Use for mode `showOnProgressChange`
```

Setup **ValueLabel** in Xml
```xml
app:ds_valueLabelTextColor="#FFF"
app:ds_valueLabelTextSize="16vp"
app:ds_valueLabelGravity="top"
app:ds_valueLabelMode="showOnPressHold|showOnProgressChange" <!-- Default is showOnPressHold -->
app:ds_valueLabelDuration="2500" <!-- Default is 1500ms -->
```
   
Setup **Progress** in Java
```java
mSlider.setCount(21);
mSlider.setProgressOffset(10);
mSlider.setProgress(5); // The same as `setMinProgress`.
mSlider.setMinProgress(5);
mSlider.setMaxProgress(10); // Only can call in range mode.
```
   
Setup **Progress** in Xml
```xml
app:ds_count="21"
app:ds_progressOffset="10"
app:ds_minProgress="5"
app:ds_maxProgress="5"
```
   
Get **Progress** in Java
```java
mSlider.getProgress(); // The same as `getMinProgress`.
mSlider.getMinProgress();
mSlider.getMaxProgress();
mSlider.setValueChangedImmediately(true); // Default is false
mSlider.setOnValueChangedListener(new DiscreteSlider.OnValueChangedListener() {

    @Override
    public void onValueChanged(int progress, boolean fromUser) {
        super.onValueChanged(progress, fromUser);
        Log.i("DiscreteSlider", "Progress: " + progress + ", fromUser: " + fromUser);
    }

    @Override
    public void onValueChanged(int minProgress, int maxProgress, boolean fromUser) {
        super.onValueChanged(minProgress, maxProgress, fromUser);
        Log.i("DiscreteSlider",
                "MinProgress: " + minProgress + ", MaxProgress: " + maxProgress +
                        ", fromUser: " + fromUser);
    }
});
```
   
Setup **Mode** in Java
```java
mSlider.setMode(DiscreteSlider.MODE_RANGE);
```
   
Setup **Mode** in Xml
```xml
app:ds_mode="range"
```
   
Setup **Click to Move** in Java (By default is `false`)
```java
mSlider.setClickable(true);
```
   
Setup **Click to Move** in Xml (By default is `false`)
```xml

```

Setup **Orientation** in Xml (By default is `horizontal`)
```xml
app:ds_orientation="vertical"
```

Setup **HapticFeedback** in Java (By default is `Enabled`)
```java
mSlider.setHapticFeedbackEnabled(false);
```




## Credits


## License

    MIT License

    Copyright (c) 2019 HearSilent

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
