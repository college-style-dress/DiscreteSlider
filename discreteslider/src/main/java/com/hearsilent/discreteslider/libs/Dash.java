package com.hearsilent.discreteslider.libs;

/**
 * Dash
 */
public class Dash {
	public float length;

	public Dash(float length) {
		super();
		this.length = Math.max(length, 0);
	}
}
